# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete('abcdefghijklmnopqrstuvwxyz')
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[str.length / 2.floor]
  else
    str[str.length / 2.floor - 1] + str[str.length / 2.floor]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |char|
    if  VOWELS.include?(char)
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  counter = num
  result = 1
  while counter > 0
    result *= counter
    counter -= 1
  end
  result
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  result = ''
  arr.each do |element|
    if element != arr.last
      result += element + separator
    else
      result += element
    end
  end
  result
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  result = ''
  str.each_char.with_index do |char, index|
    if index.even?
      result += char.downcase
    else
      result += char.upcase
    end
  end
  result
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  result = ''
  str.split().each do |word|
    if word.length > 4
      result += word.reverse + ' '
    else
      result += word + ' '
    end
  end
  result.strip
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  array = (1..n).to_a
  result = []
  array.each do |number|
    if number % 15 == 0
      result.push('fizzbuzz')
    elsif number % 5 == 0
      result.push('buzz')
    elsif number % 3 == 0
      result.push('fizz')
    else
      result.push(number)
    end
  end
  result
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  result = []
  arr.each do |element|
    result.unshift(element)
  end
  result
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  if num == 1
    return false
  end
  range = (2..num - 1).to_a
  range.each do |factor|
    if num % factor == 0
      return false
    end
  end
  true
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  result = []
  range = (1..num).to_a
  range.each do |factor|
    if num % factor == 0
      result.push(factor)
    end
  end
  result.sort
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  result = []
  factors = factors(num)
  factors.each do |factor|
    if prime?(factor)
      result.push(factor)
    end
  end
  result.sort
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  if countodds(arr) == 1
    arr.each do |num|
      if num.odd?
        return num
      end
    end
  else
    arr.each do |num|
      if num.even?
        return num
      end
    end
  end
end

def countodds(nums)
  result = []
  nums.each do |num|
    if num.odd?
      result.push(num)
    end
  end
  result.length
end
